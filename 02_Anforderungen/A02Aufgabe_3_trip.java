public class A02Aufgabe_3_trip {
    private String departure;
    private String arrival;

    public A02Aufgabe_3_trip(String departure, String arrival) {
        this.departure = departure;
        this.arrival = arrival;
    }

    // Getters and setters for departure and arrival
    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    // Method to delete both departure and arrival
    public void deleteRoute() {
        departure = null;
        arrival = null;
    }

    // Method to delete only departure
    public void deleteDeparture() {
        departure = null;
    }

    // Method to delete only arrival
    public void deleteArrival() {
        arrival = null;
    }

    @Override
    public String toString() {
        return "Departure: " + departure + ", Arrival: " + arrival;
    }
}