public class A02Aufgabe_3b {
    public static void main(String[] args) {
        A02Aufgabe_3_RaceTimer raceTimer = new A02Aufgabe_3_RaceTimer();

        
        raceTimer.countTime(40.5);
        raceTimer.countTime(38.2);
        raceTimer.saveRound();
        raceTimer.countTime(37.8);
        raceTimer.countTime(39.0);
        raceTimer.saveRound();
        raceTimer.discardTime();
        raceTimer.countTime(36.7);
        raceTimer.saveRound();

        
        System.out.println("Total Race Time: " + raceTimer.calculateRaceTime());
        System.out.println("Average Lap Time: " + raceTimer.calculateAverageTime());
    }
}
