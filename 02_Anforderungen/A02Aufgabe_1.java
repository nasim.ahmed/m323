
import java.util.Scanner;

public class A02Aufgabe_1 {
    static Scanner scanner = new Scanner(System.in);
    static Double percentage; 
    public static void main(String[] args) {
        percentage = getPercentage(numberInput());
        System.out.println(percentage);
    }
    static Double numberInput(){
        try{
            System.out.println("please input your number");
            Double number = scanner.nextDouble();
            return number; 
        } catch (Exception e){
            System.out.println(e);
        }
        return 0.0;
    }
    static double getPercentage(Double number){
        try{
            percentage = (number*95)/100;
        } catch(Exception e){
            System.out.println(e);
            percentage = 0.0;
        }
        return percentage;
    }
}
