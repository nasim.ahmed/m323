# Anforderungen als Funktionen erkennen

## 01

### Aufgabenstellung

Wir planen eine Reise durch Europa und möchten, dass der Benutzer jeweils die Destinationen für die Reise eingibt.
Zudem soll es möglich sein, eine bereits festgelegte Route zu ändern (z.Bsp. wenn Ihr Freund noch einen anderen Zwischenaufenthalt möchte).

### Mögliche funktionen

#### Reiseabteil erfassen

mit dieser funktion erfassen wir den ord der abreise und das ziel, dies kann entweder ein reise sein, bsp. wir wollen von paris nach prag, aber auch ein abteil dieser, denn wir könnten sagen wir legen einen stop in berlin ein, so haben wir 2 reisen grob gesehen, paris nach berlin und berlin nach prag

#### Abreiseabteil ändern

mit dieser funktion ändern wir den abreise ort, in unserem beispiel hatten wir paris benutz, nun wollen wir nicht von paris sondern von bordeax starten, so können wir dies hir ändern

#### Ziel ändern

mit dieser funktion ändern wir das reiseziel, im abteil war dies berlin, wollen wir nun aber über wien fahren so können wir dies hier ändern

####  Reise löshen

hier könnten wir die gesamte reise löschen

## 02

### Aufgabenstellung

Wir möchten eine App, welche für ein Auto-Rennen die gesamte Zeit für alle Runden berechnet. Die App soll auch die Durchschnittszeit pro Auto berechnen. Die erste Runde wird nicht mitgezählt, da es sich hier um eine "Warm-up" Runde handelt.

### Mögliche funktionen

#### countTime

diese funktion erfasst die zeit welche ein auto für eine runde braucht

#### saveRound

diese funktion speichert die rundenzeit

#### resetCountTimer

diese funktion setzt den timer zurück

#### discard time

diese funktion löscht die zeit von der 1. runde da die als warm up dient

#### calculateRaceTime

diese funktion berechnet die gesamtzeit für das rennen

#### calculateAverageTime

diese funktion soll die durchschnittszeit berechnen