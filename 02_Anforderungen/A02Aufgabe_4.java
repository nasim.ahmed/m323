import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class A02Aufgabe_4 {
    static TreeMap<String, Integer> map = new TreeMap<>();
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        start();
    }
    static void start(){
        System.out.println("please input what to do \n (1): Enter a word \n (2): display entries \n (3): exit");
        int input = scanner.nextInt();
        clearScanner();
        switch (input) {
            case 1:
                inputWord();
                break;
            case 2:
                printMap();
                break;
            case 3:
                System.exit(0);
                break;
            default:
                start();
                break;
        }
        start();
    }
    static void clearScanner(){
        scanner = new Scanner(System.in);
    }
    static void inputWord(){
        System.out.println("please input your word");
        String word = scanner.nextLine();
        String newWord = word.replace("a", "");
        int score = newWord.length();
        System.out.println(score + ": your word score");
        map.put(word, score);
    }
    static void printMap(){
        Map<String, Integer> sortedMap = map.entrySet()
                                                .stream()
                                                .sorted(Map.Entry.comparingByValue())
                                                .collect(LinkedHashMap::new,
                                                        (map, entry) -> map.put(entry.getKey(), entry.getValue()),
                                                        LinkedHashMap::putAll);
                                                        sortedMap.forEach((key, value) -> System.out.println(key + ": " + value));
    }
    
}
