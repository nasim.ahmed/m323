import java.util.ArrayList;
import java.util.List;

public class A02Aufgabe_3_RaceTimer {
    private List<Double> lapTimes;
    private boolean warmUpDone;

    public A02Aufgabe_3_RaceTimer() {
        lapTimes = new ArrayList<>();
        warmUpDone = false;
    }

    public void countTime(double lapTime) {
        if (warmUpDone) {
            lapTimes.add(lapTime);
        } else {
            System.out.println("Warm-up lap in progress, time not counted.");
            warmUpDone = true;
        }
    }

    public void saveRound() {
        
    }

    public void resetCountTimer() {
        lapTimes.clear();
        warmUpDone = false;
    }

    public void discardTime() {
        if (!lapTimes.isEmpty()) {
            lapTimes.remove(0); 
            warmUpDone = true;
        } else {
            System.out.println("No lap times recorded yet.");
        }
    }

    public double calculateRaceTime() {
        return lapTimes.stream().mapToDouble(Double::doubleValue).sum();
    }

    public double calculateAverageTime() {
        if (!lapTimes.isEmpty()) {
            return calculateRaceTime() / lapTimes.size();
        } else {
            return 0.0;
        }
    }
}
