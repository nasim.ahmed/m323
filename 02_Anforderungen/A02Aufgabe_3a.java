import java.util.ArrayList;


public class A02Aufgabe_3a {
    public static void main(String[] args) {

        ArrayList<A02Aufgabe_3_trip> flightRoutes = new ArrayList<>();


        flightRoutes.add(new A02Aufgabe_3_trip("New York", "London"));
        flightRoutes.add(new A02Aufgabe_3_trip("Los Angeles", "Paris"));

        flightRoutes.get(0).setDeparture("Chicago");

        
        

        
        for (A02Aufgabe_3_trip route : flightRoutes) {
            System.out.println(route);
        }
    }
    static void addOrChangeTrip(ArrayList<A02Aufgabe_3_trip> flightRoutes, String departure, String arrival, int index){
        String newArrival;
        if (flightRoutes.size() < index){
            flightRoutes.add(new A02Aufgabe_3_trip(departure, arrival));
        }  
        else if (flightRoutes.get(index).getDeparture() == departure){
            newArrival = flightRoutes.get(index).getArrival();
            flightRoutes.get(index).setArrival(arrival);
            addOrChangeTrip(flightRoutes, arrival, newArrival, index);
        }else{
            index++;
            addOrChangeTrip(flightRoutes, departure, arrival, index);
        }
    }

}
