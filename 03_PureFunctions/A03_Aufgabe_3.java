import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

public class A03_Aufgabe_3 {
    static int[] numArray = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    static String[] array = {"banana", "apple", "orange", "grape"};
    static Random random = new Random();
    static List<A03_34_Object> objects = new ArrayList<>();
    static TreeMap<String, Object> treeMap = new TreeMap<>();
    static ArrayList<String> treeArray = new ArrayList<>();
    public static void main(String[] args) {
        numArray = setupNumArray(numArray, 0);
        for (int i = 0; i < numArray.length; i++){
            System.out.println(numArray[i]);
        }
        System.out.println(calculateArray(numArray, 0, 0));
        System.out.println(calculateAverageArray(numArray, 0, 0));
        for (int i = 0; i < sortArray(array).length; i++) {
            System.out.println(sortArray(array)[i]);
        }
        addObjects();
        Collections.sort(objects, new A03_ObjectComperator());
        for (A03_34_Object obj : objects) {
            System.out.println("Date: " + obj.getDate() +
                    ", Priority: " + obj.getPriority() +
                    ", Title: " + obj.getTitle());
        }
        treeMap = setUpTreeMap(treeMap);
        extractLeafes(treeMap, treeArray, "Key 1");
        for (String tree : treeArray){
            System.out.println(tree);
        }
        
    }
    // i know this function isnt pure but it serves the purpose
    static int[] setupNumArray(int[] numArray, int index){
        if (index < numArray.length){
            numArray[index] = random.nextInt(100);
            index++;
            setupNumArray(numArray, index);
        }
        return numArray;
    }
    static int calculateArray(int[] numArray, int index, int calculation){
        if (index < numArray.length){
            calculation += numArray[index];
            index++;
            return calculateArray(numArray, index, calculation);
        }
        return calculation;
    }
    static int calculateAverageArray(int[] numArray, int index, int calculation){
        if (index < numArray.length){
            calculation += numArray[index];
            index++;
            return calculateAverageArray(numArray, index, calculation);
        }
        return calculation / numArray.length;
    }
    static String[] sortArray(String[] array){
        Arrays.sort(array);
        return array;
    }
    static void addObjects(){
        Date date = new Date();
        objects.add(new A03_34_Object(date, 2, "auto"));
        objects.add(new A03_34_Object(date, 1, "work"));
        objects.add(new A03_34_Object(date, 0, "clothes"));
        objects.add(new A03_34_Object(date, 0, "food"));
    }
    static TreeMap<String, Object> setUpTreeMap(TreeMap<String, Object> treeMap){
        treeMap.put("Key 1", null);
        treeMap.put("Key 2", null);
        treeMap.put("Key 3", new A03_34_Object(null, 0, "title 1"));
        treeMap.put("Key 4", null);
        treeMap.put("Key 5", null);
        treeMap.put("Key 6", new A03_34_Object(null, 0, "title 2"));
        treeMap.put("Key 7", null);
        treeMap.put("Key 8", null);
        treeMap.put("Key 9", null);
        treeMap.put("Key 10", new A03_34_Object(null, 0, "title 3"));
        treeMap.put("Key 11", new A03_34_Object(null, 0, "title 4"));
        treeMap.put("Key 12", new A03_34_Object(null, 0, "title 5"));
        return treeMap;
    }
    static ArrayList<String> extractLeafes(TreeMap<String, Object> treeMap, ArrayList<String> treeArray, String key){
        if (key != null){
            if (treeMap.get(key) ==null){
                treeArray.add(key);
            }
            String nextKey = treeMap.higherKey(key);
            return extractLeafes(treeMap, treeArray, nextKey);
        }
        return treeArray;
    }



}
