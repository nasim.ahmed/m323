***BE ADVISED THIS TABLE GOES OF OF JS ONLY***

| Aufgabe | nur ein rückgabewert | resultat abhängig von übergebenen parametern | verändert keine existierenden werte  | Pure Function |
|:-------:|----------------------|----------------------------------------------|--------------------------------------|:-------------:|
|1.1      |Ja                    | ja                                           | nein                                 | nein          |
|1.2      |ja                    | ja                                           | ja                                   | ja            |
|1.3      |nein                  | ja                                           | ja                                   | nein          |
|1.4      |ja                    | nein                                         | ja                                   | nein          |
|1.5      |nein                  | ja                                           | ja                                   | nein          |
|1.6      |ja                    | ja                                           | nein                                 | nein          |