import java.util.Comparator;

public class A03_ObjectComperator implements Comparator<A03_34_Object> {
    @Override
    public int compare(A03_34_Object o1, A03_34_Object o2) {
        // First, compare dates
        int dateComparison = o1.getDate().compareTo(o2.getDate());
        if (dateComparison != 0) {
            return dateComparison;
        }
        
        // If dates are equal, compare priorities
        int priorityComparison = Integer.compare(o1.getPriority(), o2.getPriority());
        if (priorityComparison != 0) {
            return priorityComparison;
        }
        
        // If priorities are equal, compare titles
        return o1.getTitle().compareTo(o2.getTitle());
    }
    
}