// aufgabe 1.1
let cartItems = [];
 function addToCart(cartItems, item){
    cartItems.push(item);
    return cartItems
 }
 console.log(addToCart(cartItems, 'Apple')); 
 console.log(addToCart(cartItems, 'Banana')); 
 console.log(addToCart(cartItems, 'Orange')); 
 // aufgabe 1.3
function firstCharacter(str) {
    try{
        return str.charAt(0);
    } catch (error){
        return "fehler"
    }
}

console.log(firstCharacter("Hello"));
console.log(firstCharacter("JavaScript"));

 // aufgabe 1.4
function multiplyWithRandom(number, randomNumber) {
    return number * randomNumber;
}

console.log(multiplyWithRandom(5, Math.random()));
console.log(multiplyWithRandom(10, Math.random())); 
// aufgabe 1.5
function divideNumbers(dividend, divisor) {
    try{
        return dividend/divisor
    } catch (error){
        return 0
    }
}

console.log(divideNumbers(10, 2));
console.log(divideNumbers(8, 4));

// aufgabe 1.6
function printAndReturnString(str) {
    return str;      
}
console.log(printAndReturnString("Hello"));
