# Pure Functions
## what defines a pure function
* returns only one value
* the return value is calculated from the values in its constructor
* no preexisting values are changed, values can be defined within the function
## advantages
* No side Effects, the function does exactly what it has been programmed to do
* Ease of testing: a pure function can be tested just by calling it
* clean code, pure functions are easy to ready as they dont have side effects
## disadvantages
* performance: the performance can be hindered as the data has to be copied
* recursion: if recursive functions are used they could use up a ton of memory