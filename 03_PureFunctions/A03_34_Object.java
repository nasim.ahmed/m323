import java.util.Date;

public class A03_34_Object {
    Date date;
    int priority;
    String title;

    A03_34_Object(Date date, int priority, String title){
        this.date = date;
        this.priority = priority;
        this.title = title;
    }
    public Date getDate() {
        return date;
    }
    public int getPriority() {
        return priority;
    }
    public String getTitle() {
        return title;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setPriority(int priority) {
        this.priority = priority;
    }
    public void setTitle(String title) {
        this.title = title;
    }

}
