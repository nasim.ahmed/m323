package A05_DatenStrukturen_Map;

import java.util.Map;
import java.util.TreeMap;

public class A05_DatenStrukturen_Map_Aufgaben {
    static Map<String, String> m1 = new TreeMap<String, String>();
    static Map<String, String> m2 = new TreeMap<String, String>();
    static Map<String, String> m3 = new TreeMap<String, String>();
    static Map<String, String> m4 = new TreeMap<String, String>();
    // Java Strings are nullable, actually
    static String valueFromM3;
    static String valueFromM4;
    public static void main(String[] args) {
        setMap(m4, "key", "value");
        setMap(m3, "key", "value");
        setMap(m3, "key2", "aDifferentValue");
        setMap(m2, "key2", "value2");
        // aufgabe 1
        setMap(m1, "key", "value");
        // aufgabe 2
        setMap(m1, m2.keySet().toArray()[0].toString(), m2.values().toArray()[0].toString());
        // aufgabe 3
        setMap(m2, m3.keySet().toArray()[0].toString(), m3.values().toArray()[0].toString());
        setMap(m2, m3.keySet().toArray()[1].toString(), m3.values().toArray()[1].toString());
        // Aufgabe 4
        removeKey(m3, m4.keySet().toArray()[0].toString(), m4.values().toArray()[0].toString());
        // Aufgabe 5
        valueFromM3 = extractValue(m3, "key2");
        valueFromM4 = extractValue(m4, null);
        System.out.println(m1 + "\n" + m2+ "\n" + m3+ "\n" + valueFromM3+ "\n" + valueFromM4);
        
    }
    static Map<String, String> setMap(Map<String, String> m, String key, String value){
        m.put(key, value);
        return m;
    }
    static Map<String, String> removeKey(Map<String, String> m, String key, String value){
        m.remove(key, value);
        return m;
    }
    static String extractValue(Map<String, String> m, String key){
        if (key == null) {
            return null;   
        }
        return m.get(key);
    }
    
}
