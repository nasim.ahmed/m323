var currentdate = new Date();
var datetime = currentdate.getDate() + "/"
+ (currentdate.getMonth()+1) + "/"
+ currentdate.getFullYear() + " @ "
+ currentdate.getHours() + ":"
+ currentdate.getMinutes() + ":"
+ currentdate.getSeconds();
const weather = [
    {"city":"zürich", "weather" : "cloudy", "temperature" : 11.4, "date" :  datetime },
    {"city":"london", "weather" : "partly cloudy", "temperature" : 13.5, "date" :  datetime },
    {"city":"paris", "weather" : "sunny", "temperature" : 17.8, "date" :  datetime },
    {"city":"new york", "weather" : "rainy", "temperature" : 15.2, "date" :  datetime },
    {"city":"berlin", "weather" : "windy", "temperature" : 14.9, "date" :  datetime },
    {"city":"tokyo", "weather" : "foggy", "temperature" : 19.6, "date" :  datetime },
    {"city":"sydney", "weather" : "snowy", "temperature" : 8.7, "date" :  datetime },
    {"city":"moscow", "weather" : "clear", "temperature" : 10.3, "date" :  datetime },
    {"city":"rome", "weather" : "partly sunny", "temperature" : 16.1, "date" :  datetime },
    {"city":"istanbul", "weather" : "thunderstorms", "temperature" : 20.5, "date" :  datetime },
    {"city":"miami", "weather" : "sunny", "temperature" : 28.9, "date" :  datetime },
    {"city":"dubai", "weather" : "clear", "temperature" : 33.7, "date" :  datetime },
    {"city":"bangkok", "weather" : "partly cloudy", "temperature" : 31.4, "date" :  datetime }
];
console.log(weather)
const warmWeather = weather.filter(weather => weather.temperature >= 20)
console.log(warmWeather)