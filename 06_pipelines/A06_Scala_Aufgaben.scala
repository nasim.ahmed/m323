package M323.A06_WeitereAlgorithmen
import cats.effect.IO
import cats.effect.unsafe.implicits.global
import scala.util.Random

case class Course(title: String, students: List[String])
class A06Aufgabe1{
    val courses = List(
        Course("M323", List("Peter", "Petra", "Paul", "Paula")),
        Course("M183", List("Paula", "Franz", "Franziska")),
        Course("M117", List("Paul", "Paula")),
        Course("M114", List("Petra", "Paul", "Paula")),
    )
    def getCourses(): List[Course] ={
        courses
    }
}
class A06Aufgabe2{
    val allowToLeaf = allowToLeafHome

    def allowToLeafHome: IO[Boolean] = {
        val checkRoll = rollDice()
        checkRoll.map(_ == 6)
    }
    def rollDice(): IO[Int] = {
    IO.delay(rollDiceImpure())
    }
    def rollDiceImpure(): Int = {
    val random = new Random
    random.nextInt(6) + 1
}


}
class A06Aufgabe3{
    val stream1 = Stream(1,2,3)
    val stream2 = Stream(4,5,6)
    val stream3 = stream1.append(stream2)
    val streamInfinite = numbers()
    val streamInfinite2 = Stream(1,2,3)


    def tolist(): List[Int] ={
        stream1.toList 
    }
    def tolist3(): List[Int] ={
        stream3.toList 
    }
    def takeFive(): List[Int]={
        stream1.take(5).toList
    }
    def getStream1(): Stream[Int] ={
        stream1
    }
    def getStream2(): Stream[Int] ={
        stream2
    }
    def getStream3(): Stream[Int] ={
        stream3
    }
    def numbers(): Stream[Int] = {
        Stream(1,2,3).append(numbers()) 
    }
    def getStreamInfinite(): List[Int] ={
        streamInfinite.take(8).toList
    }

    def getStreamInfinite2(): List[Int] ={
        streamInfinite2.take(8).toList
    }

}
object Main {
    val aufgabe1 = new A06Aufgabe1();
    val aufgabe2 = new A06Aufgabe2();
    val aufgabe3 = new A06Aufgabe3();
    @main def run(): Unit = {
        // Aufgabe 1 (Pipelines)
        println(aufgabe1.getCourses().filter(_.students.contains("Peter")).map(_.title))
        println(aufgabe1.getCourses().filter(_.students.contains("Petra")).map(_.title))
        // Aufgabe 2 (IO)
        println(aufgabe2.allowToLeaf.unsafeRunSync())
        // Aufgabe3
        println(aufgabe3.tolist())
        println(aufgabe3.takeFive())
        println(aufgabe3.tolist3())
        println(aufgabe3.getStreamInfinite())
        println(aufgabe3.getStreamInfinite2())
    }
}
