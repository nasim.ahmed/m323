package Aufgabe_2;

import java.util.ArrayList;

class ShoppingCart{
        private ArrayList items = new ArrayList<>();
        private boolean bookAdded;
        public void addItem(String CartItem){
            items.add(CartItem);
        }
        public void removeItem(String CartItem){
            items.remove(CartItem);
        }
        public void addBook(String Bookname){
            Book book = new Book(Bookname);
            items.add(book);
            bookAdded = true;
        }
        public boolean eligableForDiscount(ArrayList items){
            for (int item = 0 ; item < items.size(); item++){
                if (items.get(item) instanceof Book){
                    return true;
                }
            }
            return false;
        }
        public int getDiscountPercentage(){
            if (eligableForDiscount(items)){
                return 5;
            }
            return 0;
        }
        public static void main(String[] args) {
            
        }
    }