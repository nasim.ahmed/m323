import java.util.ArrayList;
import java.util.List;

public class Aufgabe_3 {
    private static List<String> names = new ArrayList<>();

    public static void addPerson(String name) {
        names.add(name);
    }

    public static List<String> getNames() {
        return names;
    }

    public static int getTipPercentage() {
        return calculateTipPercentage();
    }
    public static int calculateTipPercentage(){
        if (names.size() == 0){
            return 0;
        } else if (names.size() >= 5){
            return 20;
        } else if (names.size() <=5){
            return 10;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(getTipPercentage());
        addPerson("a");
        System.out.println(getTipPercentage());
        addPerson("b");
        addPerson("c");
        addPerson("d");
        addPerson("e");
        addPerson("f");
        System.out.println(getTipPercentage());
    }
}
