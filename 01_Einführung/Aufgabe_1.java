public class Aufgabe_1 {
    public static void main(String[] args) {
        System.out.println(calculatedScore("imperative"));
        System.out.println(calculatedScore("no"));
        System.out.println(wordScore("declarative"));
        System.out.println(wordScore("yes"));
    }

    public static int calculatedScore(String word) {
        int score = 0;
        for (char c : word.toCharArray()){
            if (c != 'a'){
                score++;
            }
            
        }
        return score;
    }
    public static int wordScore(String word){
        word = word.replace("a", "");
        return word.length();
    }
        
}