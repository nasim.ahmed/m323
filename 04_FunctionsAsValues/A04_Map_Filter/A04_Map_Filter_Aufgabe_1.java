package A04_Map_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Filter_Aufgabe_1 {
    static List<A04_CoworkerObject> coworkers = Arrays.asList(
        new A04_CoworkerObject("Max Mustermann", "IT", 50000),
        new A04_CoworkerObject("Erika Musterfrau", "Marketing", 45000),
        new A04_CoworkerObject("Klaus Klein", "IT", 55000),
        new A04_CoworkerObject("Julia Gross", "HR", 40000)
        );
    public static void main(String[] args) {
        System.out.println(coworkersWorthOverFiftyThousand(coworkers));
    }
    static List<String> coworkersWorthOverFiftyThousand(List<A04_CoworkerObject> coworkers){
        List<String> filtered = coworkers.stream()
                                .filter(t -> t.getGehalt() >= 50000)
                                .map(t -> t.getName().split(" ")[0].toUpperCase())
                                .collect(Collectors.toList());
        return filtered;
        }
}
