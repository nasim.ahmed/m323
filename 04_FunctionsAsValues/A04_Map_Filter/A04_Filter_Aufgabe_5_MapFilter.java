package A04_Map_Filter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
public class A04_Filter_Aufgabe_5_MapFilter {
    static List<A04_BookObject> buecher = Arrays.asList(
        new A04_BookObject("1984", "George Orwell", 1949),
        new A04_BookObject("Brave New World", "Aldous Huxley", 1932),
        new A04_BookObject("Fahrenheit 451", "Ray Bradbury", 1953)
        );
    public static void main(String[] args) {
        System.out.println(FindBooksBeforeFifty(buecher));

    }
    static List<String> FindBooksBeforeFifty(List<A04_BookObject> buecher){
        List<String> filtered = buecher.stream()
                                    .filter(t -> t.getRelease() <= 1950)
                                    .map(t -> t.getName())
                                    .collect(Collectors.toList());
        return filtered;
    }
}   
