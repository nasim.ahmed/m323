package A04_Map_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Filter_Aufgabe_2 {
    static List<String> kurse = Arrays.asList(
        "Programmierung in Scala",
        "Datenbanken",
        "Webentwicklung mit JavaScript",
        "Algorithmen und Datenstrukturen"
    );
    public static void main(String[] args) {
        System.out.println(findDataWord(kurse));
        System.out.println(removeSpaces(kurse));
        System.out.println(alphabetSort(kurse));
        System.out.println(reverseAlphabetSort(kurse));
    }
    static List<String> findDataWord(List<String> kurse){
        List<String> filtered = kurse.stream()
                                .filter(t -> t.contains("Daten"))
                                .collect(Collectors.toList());
        return filtered;
    }
    static List<String> removeSpaces(List<String> kurse){
        List<String> filtered = kurse.stream()
                                .map(t -> t.replace(" ", ""))
                                .collect(Collectors.toList());
        return filtered;
    }
    static List<String> alphabetSort(List<String> kurse){
        List<String> filtered = kurse.stream()
                                .sorted()
                                .collect(Collectors.toList());
        return filtered;
    }
    static List<String> reverseAlphabetSort(List<String> kurse){
        List<String> filtered = kurse.stream()
                                .sorted((a, b) -> b.compareTo(a))
                                .collect(Collectors.toList());
        return filtered;
    }
    }
