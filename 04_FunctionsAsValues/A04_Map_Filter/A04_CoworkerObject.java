package A04_Map_Filter;

public class A04_CoworkerObject {
    String name;
    String abteilung;
    int gehalt;
    public A04_CoworkerObject(String nameString, String abteilungString, int gehaltInt){
        this.name = nameString;
        this.abteilung = abteilungString;
        this.gehalt = gehaltInt;
    }
    public String getAbteilung() {
        return abteilung;
    }
    public String getName() {
        return name;
    }
    public int getGehalt() {
        return gehalt;
    }
    public void setAbteilung(String abteilung) {
        this.abteilung = abteilung;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setGehalt(int gehalt) {
        this.gehalt = gehalt;
    }
    
}
