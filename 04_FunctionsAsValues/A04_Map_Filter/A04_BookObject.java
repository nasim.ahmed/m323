package A04_Map_Filter;

public class A04_BookObject {
    String name;
    String author;
    int release;
    public A04_BookObject(String nameString, String authorString, int releaseInt){
        this.name = nameString;
        this.author = authorString;
        this.release = releaseInt;
    }
    public String getAuthor() {
        return author;
    }
    public String getName() {
        return name;
    }
    public int getRelease() {
        return release;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setRelease(int release) {
        this.release = release;
    }
    
}
