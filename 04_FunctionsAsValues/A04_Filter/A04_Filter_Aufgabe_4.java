package A04_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Filter_Aufgabe_4 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Scala", "ist", "fantastisch");
        List<String> fourOrMoreLetterNAmes = names.stream()
                                           .filter(n -> n.charAt(0) == 'S')
                                           .collect(Collectors.toList());

           

         System.out.println(fourOrMoreLetterNAmes);
        
    }
}
