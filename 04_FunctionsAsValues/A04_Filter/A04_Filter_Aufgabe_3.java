package A04_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Filter_Aufgabe_3 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(12, 45, 68, 100);
        List<Integer> overFifty = numbers.stream()
                                           .filter(n -> n >= 50)
                                           .collect(Collectors.toList());

           

         System.out.println(overFifty);
        
    }
}
