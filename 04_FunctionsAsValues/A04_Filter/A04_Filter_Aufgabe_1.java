package A04_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Filter_Aufgabe_1 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> evenNumbers = numbers.stream()
                                           .filter(n -> n % 2 == 0)
                                           .collect(Collectors.toList());

           

         System.out.println(evenNumbers);
        
    }
}
