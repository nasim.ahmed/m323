package A04_Filter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Filter_Aufgabe_2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "Diana");
        List<String> fourOrMoreLetterNAmes = names.stream()
                                           .filter(n -> n.length() >= 4)
                                           .collect(Collectors.toList());

           

         System.out.println(fourOrMoreLetterNAmes);
        
    }
}
