
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Aufgabe_2 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

       
        List<String> uppercaseNames = names.stream()
                                              .map(String::toUpperCase)
                                              .collect(Collectors.toList());

        System.out.println(uppercaseNames);
    }
}
