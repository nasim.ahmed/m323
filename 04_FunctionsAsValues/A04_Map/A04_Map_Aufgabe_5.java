
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Aufgabe_5 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Max Mustermann", "Erika Mustermann");

       
        List<String> uppercaseNames = names.stream()
                                              .map(n -> n.split(" ")[0].toUpperCase())
                                              .collect(Collectors.toList());

        System.out.println(uppercaseNames);
    }
}
