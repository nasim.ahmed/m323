
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Aufgabe_4 {
    public static void main(String[] args) {
        List<Adresse> adressen = Arrays.asList(
            new Adresse("Hauptstrasse", 10, "12345", "Musterstadt"),
            new Adresse("Nebenstrasse", 5, "23456", "Beispielburg")
        );

       
        List<String> combinierteAdresse = adressen.stream()
                                              .map(t ->  t.getStrasse() + " " + t.getHausnummer() + ", " + t.getPostleitzahl() + " " + t.getStadt())
                                              .collect(Collectors.toList());

        System.out.println(combinierteAdresse);
    }
}
class Adresse{
    
String strasse; 
int hausnummer; 
String postleitzahl; 
String stadt;

    Adresse(String strasse, int hausnummer, String postleitzahl, String stadt){
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.postleitzahl = postleitzahl;
        this.stadt = stadt;
    }
    public int getHausnummer() {
        return hausnummer;
    }
    public String getPostleitzahl() {
        return postleitzahl;
    }
    public String getStadt() {
        return stadt;
    }
    public String getStrasse() {
        return strasse;
    }
    public void setHausnummer(int hausnummer) {
        this.hausnummer = hausnummer;
    }
    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }
    public void setStadt(String stadt) {
        this.stadt = stadt;
    }
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

}