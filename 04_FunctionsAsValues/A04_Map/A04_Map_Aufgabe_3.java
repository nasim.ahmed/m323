
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_Map_Aufgabe_3 {
    public static void main(String[] args) {
        List<Float> numbers = Arrays.asList(12f, 45f, 68f, 100f);

       
        List<Float> halvedNumbers = numbers.stream()
                                              .map(n -> n / 2)
                                              .collect(Collectors.toList());

        System.out.println(halvedNumbers);
    }
}
