package A04_FoldLeft;

import java.util.Arrays;
import java.util.List;

public class A04_FoldLeft_Aufgabe1 {
    static List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
    static List<String> strings = Arrays.asList("Hallo", " ", "Welt", "!");
    public static void main(String[] args) {
        System.out.println(foldNumbers(numbers));
        System.out.println(foldString(strings));
     }
    static int foldNumbers(List<Integer> numbers){
        int sum = numbers.stream()
                        .reduce(0, (acc, num) -> acc + num);
        return sum;
     }
    static String foldString(List<String> strings){
        String concatenatedString = strings.stream()
                                          .reduce("", (acc, str) -> acc + str);
        return concatenatedString;
    }

}
