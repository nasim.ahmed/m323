function getAverage(points, index, average) {
    if (index >= points.length) {
        if (average.x === 0 || average.y === 0 || index === 0 || average.x === null || average.y === null) {
            return null;
        }
        return { x: average.x / index, y: average.y / index }; 
    }
    const newAverage = { x: average.x + points[index].x, y: average.y + points[index].y };
    return getAverage(points, index + 1, newAverage);
}

const points = [{ x: 1, y: 3 }, { x: 2, y: 5 }, { x: 4, y: 8 }, { x: 6, y: 2 }];
const average = getAverage(points, 0, { x: 0, y: 0 });
console.log(average);