const peopleAndFavColour = [
    { name: "Max", colors: ["Blau", "Grün"] },
    { name: "Anna", colors: ["Rot"] },
    { name: "Julia", colors: ["Gelb", "Blau", "Grün"] }
  ];
function getColours(peopleAndFavColour){
    const colourArray = peopleAndFavColour.flatMap(person => person.colors);
    return colourArray;
}

const colours = getColours(peopleAndFavColour);
console.log(colours);