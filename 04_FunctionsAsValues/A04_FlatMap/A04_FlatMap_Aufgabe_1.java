package A04_FlatMap;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class A04_FlatMap_Aufgabe_1 {
    static List<List<Integer>> listOfLists = Arrays.asList(
                Arrays.asList(1, 2),
                Arrays.asList(3, 4),
                Arrays.asList(5, 6)
        );
    public static void main(String[] args) {
        System.out.println(doubleEveryNumber(listOfLists));
    }
    static List<Integer> doubleEveryNumber(List<List<Integer>> listOfLists){
            return listOfLists.stream()
                .flatMap(innerList -> innerList.stream().map(n -> n * 2))
                .collect(Collectors.toList());    }
}
