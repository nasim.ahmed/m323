
// Aufgabe 1
function range(end) {
    return Array.from({ length: end }, (_, i) => i + 1);
}
function squareEach(numbers){
    return numbers.map(n => n*n)
}  
const numbersA1 = range(10);
console.log("Aufgabe 1 \n"+squareEach(numbersA1))

// Aufgabe 2
function returnEven(numbers){
    return numbers.filter(n => n%2 === 0)
}
const numbersA2 = range(20);
console.log("Aufgabe 2 \n"+returnEven(numbersA2))

// Aufgabe 3
const colors = ["Red", "Green", "Blue"];
const fruits = ["Apple", "Banana", "Orange"];

function getCombos(colors, fruits) {
    return colors.flatMap(color => {
        return fruits.map(fruit => ({ color, fruit }));
    });
}
const combos = getCombos(colors, fruits);
console.log(combos);

// Aufgabe 4
class User {
    constructor(name, tasks) {
        this.name = name;
        this.tasks = tasks;
    }
}

const users = [
    new User("Alice", ["Task 1", "Task 2", "Task 3"]),
    new User("Bob", ["Task 1", "Task 4", "Task 5"]),
    new User("Charlie", ["Task 2", "Task 3", "Task 6"])
];

const tasksCompleted = ["Task 1", "Task 3", "Task 5"];

function findIncompleteTasks(users, tasksCompleted) {
    let incomplete = users.map(user => {
        let incompleteTasks = user.tasks.filter(task => !tasksCompleted.includes(task));
        return { user: user.name, incompleteTasks: incompleteTasks };
    });
    return incomplete;
}

console.log(findIncompleteTasks(users, tasksCompleted));

